Revocalize.Player = new (new Class({
	Extends: Revocalize,
	Implements: [Events, Options],
	Binds: [
		'activate',
		'keyFormSubmit',
		'keyPadKeyUp',
		'loginSubmit',
		'loginSubmitSuccess',
		'loginSubmitFailure',
		'loadScriptSuccess',
		'loadScriptFailure'
		],
	options: {
		failedToLoadMessage: "Failed to load the script.",
		iframeHelperURL: 'iframe-helper.php',
		authenticationURL: 'auth.php',
		loadScriptURL: 'load.php',
		activeClassName: 'active',
		
		hotKeyTemplate:
		'<div class="hotkey-item" keycode="{keycode}" id="hotkey-{keycode}">\
			<div class="keycode">{keycode}</div>\
			<div class="title">{title}</div>\
		</div>',
		
		scriptTemplate:
		'<div class="script-group" id="script-group-{keycode}">\
			<div class="script-item {className}" keycode="{keycode}" id="{script_prefix}{keycode}">\
				<div class="keycode">{keycode}</div>\
				<div class="text">{text}</div>\
			</div>\
			<div class="script-responses">\
				{responsesHTML}\
			</div>\
		</div>',
		
		scriptResponseTemplate:
		'<div class="script-response-item {className}" keycode="{keycode}" id="script-response-{keycode}">\
			<div class="keycode">{keycode}</div>\
			<div class="text">{text}</div>\
		</div>',
		
		rebuttalsTemplate:
		'<div class="script-item {className}" keycode="{keycode}" id="{script_prefix}{keycode}">\
			<div class="keycode">{keycode}</div>\
			<div class="text">{text}</div>\
		</div>',
		
		authenticateRepTemplate:
		'<form >\
			<div>\
				<label for="rep_id">ID:</label>\
				<input class="form-control" type="text" name="rep_id" id="rep_id" />\
			</div>\
			<div>\
				<label for="password">Password:</label>\
				<input class="form-control" type="password" name="password" id="password" />\
			</div>\
			<input class="btn btn-default" type="submit" value="submit" />\
		</form>'
	},
	
	initialize: function(o)
	{
		o = o || {};
		this.parent(o);
		window.addEvent('domready' , this.activate);
	},
	
	activate: function()
	{
		
		this.activateElements();
		if (Revocalize.Config && Revocalize.Config.rep_id)
		{
			this.repID = Revocalize.Config.rep_id;
			this.loadScript();
		}
		else
		{
			this.showLogin();
		}
	},
	
	activateElements: function()
	{
		this.body = $$('body')[0];
		this.keyForm = $('keyForm');
		this.keyPad = $('keyPad');
		this.hotKeysElement = $('hotkeys');
		this.scriptElement = $('script');
		
		this.keyForm.addEvent('submit' , this.keyFormSubmit);
		this.keyPad.addEvent('keyup' , this.keyPadKeyUp);
	},
	
	showLogin: function()
	{
		this.loginForm = Elements.from(this.options.authenticateRepTemplate);
		this.showScrimOverlay({html: this.loginForm , className: "form"});
		this.loginForm.addEvent('submit' , this.loginSubmit);
		var rep_id = this.loginForm.getElementById('rep_id')[0];
		rep_id.focus();
	},
	
	loginSubmit: function(evt)
	{
		var rep = this.loginForm.getElement('input[name=rep_id]')[0],
			pass = this.loginForm.getElement('input[name=password]')[0],
			form = this.loginForm;
		if (rep.value == "" || pass.value == "")
		{
			form.addClass('error');
			window.setTimeout(function() {
				form.removeClass('error');
			} , 800);
			return false;
		}
		
		new Request.JSON({
			url: this.options.authenticationURL,
			data: {
				rep_id: rep.value,
				password: pass.value
			},
			method: "POST",
			onSuccess: this.loginSubmitSuccess,
			onFailure: this.loginSubmitFailure
		}).send();
		this.showLoading("Authenticating...");
		return false;
	},
	
	loginSubmitSuccess: function(t)
	{
		if (t && t.rep_id)
		{
			this.hideLoading();
			this.hideScrimOverlay();
			delete this.loginForm;
			this.repID = t.rep_id;
			this.loadScript();
		}
		else
		{
			this.loginSubmitFailure();
		}
	},
	
	loginSubmitFailure: function()
	{
		this.hideLoading();
		this.showScrimOverlay({html: this.loginForm , className: "form"});
		this.loginForm.addClass('error');
		var form = this.loginForm;
		window.setTimeout(function() {
			form.removeClass('error');
		} , 800);
		return false;
	},
	
	loadScript: function()
	{
		new Request({
			url: this.options.loadScriptURL,
			method: "GET",
			onSuccess: this.loadScriptSuccess,
			onFailure: this.loadScriptFailure
		}).send();
	},
	
	loadScriptSuccess: function(c , xml)
	{
		if (!xml)
		{
			this.loadScriptFailure();
		}
		
		this.processScripts(xml.getElements('script nodes node'));
		this.processRebuttals(xml.getElements('script rebuttals rebuttal'));
		this.processHotKeys(xml.getElements('script hotkeys hotkey'));
		this.keyPad.focus();
	},
	
	loadScriptFailure: function()
	{
		this.errorDialog(this.options.failedToLoadMessage);
	},
	
	processScripts: function(scripts)
	{
		var scriptsHTML = [];
		this.script = [];
		scripts.each(function(hk) {
			var script = this.processUniversalElement(hk);
			if (script)
			{
				this.script.push(script);
				scriptsHTML.push(this.buildScripts(script));
			}
		} , this);
		this.scriptElement.set('html' , scriptsHTML.join(''));
	},
	
	buildScripts: function(script)
	{
		var responsesHTML = [];
		if (script.responses && script.responses.length > 0)
		{
			script.responses.each(function(r) {
				responsesHTML.push(this.buildScriptsResponses(r));
			} , this);
		}
		return this.options.scriptTemplate.substitute($H(script).combine({
			keycode: script.keycode.escapeHTML(),
			text: script.visibleText.escapeHTML(),
			script_prefix: 'script-',
			responsesHTML: responsesHTML.join('')
		}));
	},
	
	buildScriptsResponses: function(response)
	{
		return this.options.scriptResponseTemplate.substitute($H(response).combine({
			keycode: response.keycode.escapeHTML(),
			text: response.visibleText.escapeHTML()
		}));
	},
	
	processRebuttals: function(rebuttals)
	{
		var rebuttalsHTML = [];
		this.rebuttals = [];
		rebuttals.each(function(hk) {
			var rebuttal = this.processUniversalElement(hk);
			if (rebuttal)
			{
				this.rebuttals.push(rebuttal);
				rebuttalsHTML.push(this.buildRebuttals(rebuttal));
			}
		} , this);
		this.scriptElement.adopt(Elements.from(rebuttalsHTML.join('')));
	},
	
	buildRebuttals: function(script)
	{
		return this.options.scriptTemplate.substitute($H(script).combine({
			keycode: script.keycode.escapeHTML(),
			text: script.visibleText.escapeHTML(),
			script_prefix: 'rebuttal-',
			className: 'rebuttal'
		}));
	},
	
	processHotKeys: function(hotKeys)
	{
		var hotkeyHTML = [];
		this.hotKeys = [];
		hotKeys.each(function(hk) {
			
			var hotKey = this.processUniversalElement(hk);
			if (hotKey)
			{
				this.hotKeys.push(hotKey);
				hotkeyHTML.push(this.buildHotKey(hotKey));
			}
		} , this);
		this.hotKeysElement.set('html' , hotkeyHTML.join(''));
	},
	
	buildHotKey: function(hotKey)
	{
		return this.options.hotKeyTemplate.substitute($H(hotKey).combine({
			keycode: hotKey.keycode.escapeHTML(),
			title: hotKey.visibleText.escapeHTML()
		}));
	},
	
	processUniversalElement: function(node)
	{
		var el = {},
			properties = ['keycode' , 'audioFile' , 'autoGoto' , 'visibleText' , 'invisibleText'];
			multiples = [{property: 'audioFiles' , children: 'audioFile'},{property: 'responses' , children: 'response' , full: true}];
		properties.each(function(n) {
			var nel = node.getElement(n);
			if (nel && nel.parentNode == node)
			{
				el[n] = nel.textContent;
			}
		} , this);
		multiples.each(function(n) {
			var nels = node.getElements(n.property + " " + n.children);
			
			if (nels && nels.length > 0)
			{
				el[n.property] = [];
				nels.each(function(ch) {
					if (ch.parentNode.parentNode == node) // check if its properly nested.
					{
						if (n.full)
						{
							el[n.property].push(this.processUniversalElement(ch));
						}
						else
						{
							el[n.property].push(ch.textContent);
						}
					}
				} , this);
			}
		} , this);
		return el;
	},
	
	keyPadKeyUp: function(evt)
	{
		var value = this.keyPad.get('value').toLowerCase();
		if (this.processKeyCode(value) === false)
		{
			this.clearKeyPad();
			return false;
		}
	},
	
	processKeyCode: function(keyCode)
	{
		var signal,
			multiples = ['t' , 'z' , 'r'];
		if (keyCode.length === 1 && multiples.indexOf(keyCode) == -1)
		{
			signal = this.getSignalForKeyCode(keyCode);
			if (!signal)
			{
				return false;
			}
			else
			{
				if (keyCode === "0")
				{
					this.resetSignals();
				}
				this.processSignal(signal);
			}
		}
		else if ((keyCode.length == 2 || keyCode.length == 3) && multiples.indexOf(keyCode.substr(0 , 1)) != -1)
		{
			if (keyCode.substr(0 , 1) == "z")
			{
				signal = this.getScriptForKeyCode(keyCode);
				if (!signal)
				{
					if (keyCode.length == 2)
					{
						return;
					}
					return false;
				}
				else
				{
					this.processScript(signal);
					return;
				}
			}
			else if (keyCode.substr(0 , 1) == "r")
			{
				signal = this.getRebuttalForKeyCode(keyCode);
			}
			else if (keyCode.substr(0 , 1) == "t")
			{
				signal = this.getSignalForKeyCode(keyCode);
			}
			
			if (!signal)
			{
				return false;
			}
			else
			{
				this.processSignal(signal);
			}
		}
		else if (keyCode.length > 1)
		{
			return false;
		}
	},
	
	getSignalForKeyCode: function(keyCode)
	{
		keyCode = keyCode.toLowerCase();
		var signal = this.getResponseForKeyCode(keyCode);
		if (signal)
		{
			return signal;
		}
		return this.getHotKeyForKeyCode(keyCode);
	},
	
	getHotKeyForKeyCode: function(keyCode)
	{
		return this.getSignalFrom(this.hotKeys , keyCode);
	},
	
	getRebuttalForKeyCode: function(keyCode)
	{
		return this.getSignalFrom(this.rebuttals , keyCode);
	},
	
	getScriptForKeyCode: function(keyCode)
	{
		return this.getSignalFrom(this.script , keyCode);
	},
	
	getResponseForKeyCode: function(keyCode)
	{
		if (!this.activeScript)
		{
			return false;
		}
		else
		{
			return this.getSignalFrom(this.activeScript.responses , keyCode);
		}
	},
	
	getSignalFrom: function(list , keyCode)
	{
		if (!$chk(list) || $type(list) != 'array')
		{
			return false;
		}
		
		keyCode = keyCode.toLowerCase();
		for(var i=0,z=list.length,sig; i < z; i++)
		{
			sig = list[i];
			if (sig.keycode.toLowerCase() == keyCode)
			{
				return sig;
			}
		}
		return false;
	},
	
	resetSignals: function()
	{
		this.hotKeys.each(function(hk) {
			if (hk.audioFiles && $type(hk.audioFiles) == 'array' && hk.audioFile)
			{
				delete hk.audioFile;
			}
		} , this);
	},
	
	processScript: function(script)
	{
		this.clearKeyPad();
		this.activeScript = script;
		this.processAudio(script);
		$$('.script-group.active').each(function(ac) {
			ac.removeClass(this.options.activeClassName);
		} , this);
		var els = $('script-group-' + script.keycode);
		if (els)
		{
			els.addClass(this.options.activeClassName);
			els.scrollIntoView()
		}
	},
	
	processSignal: function(signal)
	{
		this.clearKeyPad();
		this.processAudio(signal);
	},
	
	processAudio: function(signal)
	{
		this.stopAudio();
		if (signal.audioFiles && $type(signal.audioFiles) == 'array')
		{
			var ind = signal.audioFiles.indexOf(signal.audioFile);
			if (!signal.audioFile || ind == -1 || ind == (signal.audioFiles.length-1))
			{
				signal.audioFile = signal.audioFiles[0];
			}
			else
			{
				signal.audioFile = signal.audioFiles[ind+1];
			}
		}
		
		if (signal.audioFile)
		{
			baseAudioURL="/nonvoice/scripts/";
			if (signal.autoGoto)
			{
				this.playAudio(baseAudioURL + signal.audioFile , this.processAutoGoto.bind(this , signal));
			}
			else
			{
				this.playAudio(baseAudioURL + signal.audioFile);
			}
		}
		else
		{
			if (signal.autoGoto)
			{
				this.processAutoGoto(signal);
			}
		}
	},
	
	processAutoGoto: function(signal)
	{
		this.processKeyCode(signal.autoGoto);
	},
	
	clearKeyPad: function()
	{
		this.keyPad.set('value' , '');
	},
	
	keyFormSubmit: function(evt)
	{
		return false;
	}
	
}))();

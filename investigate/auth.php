<?php
require_once('../settings.php');

$repID = $_POST['rep_id'];
if (!$repID)
{
	header("HTTP/1.1 403 Forbidden");
	die("Invalid Rep ID Format");
}
if (isValidRep($repID , $_POST['password']))
{
	$_SESSION['user_id'] = $repID;
	echo json_encode(array('rep_id' => $repID));
}
else
{
	header("HTTP/1.1 403 Forbidden");
	die("Authentication Failed");
}

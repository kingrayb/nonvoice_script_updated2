<?php

define("BLuewaterLLC", "77447");

function obfuscateActionID($friendlyActionID)
{
	if ($friendlyActionID == "ADMIN_REP_ADD_SUBMIT") { return("936351054"); } else
	if ($friendlyActionID == "ADMIN_REP_UPDATE_SUBMIT") { return("628452945"); }
}

function deobfuscateActionID($unfriendlyActionID)
{
	if ($unfriendlyActionID == "936351054") { return("ADMIN_REP_ADD_SUBMIT"); } else
	if ($unfriendlyActionID == "628452945") { return("ADMIN_REP_UPDATE_SUBMIT"); } else
	{ return(""); } //must not be a legal obfuscated action ID
}

function repAdd()
{
	sleep(1); //attack deterrent
	//get post vars
	openDb();
	$repID=mysql_real_escape_string($_POST['fieldRepID']);
	$repPW=mysql_real_escape_string($_POST['fieldRepPW']);
	$repName=mysql_real_escape_string($_POST['fieldRepName']);
	$repScriptID=mysql_real_escape_string($_POST['fieldRepScriptID']);
	$repStatus=mysql_real_escape_string($_POST['fieldRepStatus']);
	$submitter=mysql_real_escape_string($_POST['fieldSubmitter']);
	//validate required fields
	if ( ($submitter==SAVANTIUS) && ($repName!="") && ($repID!="") && ($repPW!="") && ($repStatus!="") )
	{
		//add rec to db
		date_default_timezone_get("America/Boise");
		putenv("TZ=America/Boise");
		$repAddedDateTime=time(); //unix date time
		$repUpdatedDateTime=time(); //unix date time
		$submitterIP=$_SERVER['REMOTE_ADDR'];
    $result = mysql_query("INSERT INTO `nonvoice_agc` (`userid`, `password`, `Name`, `status`, `script_id`, `registered_datetime`, `modified_datetime`, `submitterIP`) 
																	 VALUES ('$repID', '$repPW', '$repName', '$repStatus', '$repScriptID', '$repAddedDateTime', '$repUpdatedDateTime', '$submitterIP')");
	}
	closeDb();
	if ($result)
	{
		return(1);
	}
	else
	{
		return(0);
	}
}

function repUpdate()
{
	sleep(1); //attack deterrent
	//get post vars
	openDb();
	$recID=mysql_real_escape_string($_POST['fieldRecID']);
	$repID=mysql_real_escape_string($_POST['fieldRepID']);
	$repPW=mysql_real_escape_string($_POST['fieldRepPW']);
	$repName=mysql_real_escape_string($_POST['fieldRepName']);
	$repStatus=mysql_real_escape_string($_POST['fieldRepStatus']);
	$repScriptID=mysql_real_escape_string($_POST['fieldRepScriptID']);
	$submitter=mysql_real_escape_string($_POST['fieldSubmitter']);
	//validate required fields
	if ( ($submitter==SAVANTIUS) && ($repName!="") && ($repID!="") && ($repPW!="") && ($repStatus!="") )
	{
		//update rec in db
		date_default_timezone_get("America/Boise");
		putenv("TZ=America/Boise");
		$repUpdatedDateTime=time(); //unix date time
		$submitterIP=$_SERVER['REMOTE_ADDR'];
		$result = mysql_query("UPDATE `nonvoice_agc` SET `userid`='$repID', `password`='$repPW', `Name`='$repName', `status`='$repStatus', `script_id`='$repScriptID', `modified_datetime`='$repUpdatedDateTime', `submitterIP`='$submitterIP' WHERE `id`='$recID'");
	}
	closeDb();
	if ($result)
	{
		return(1);
	}
	else
	{
		return(0);
	}
}

function showRepList()
{
	$content="
	<table class='table table-striped'>
		<thead>
		  <tr id='repTblRowHeader'>	
			<th>Name</th>
			<th>User ID</th>
			<th>Password</th>
			<th>Script ID</th>
			<th>Status</th>
			<th>Action</th>
	   	  </tr>
		</thead>
		<tr class='repTblRowAdd'>	
			<form name='formAddRep' method='post' action='./?a=".obfuscateActionID('ADMIN_REP_ADD_SUBMIT')."'>
			<td><input type='text' class='repTblCell form-control' name='fieldRepName' value='' /></td>
			<td><input type='text' class='repTblCell form-control' name='fieldRepID' value='' /></td>
			<td><input type='text' class='repTblCell form-control' name='fieldRepPW' value='' /></td>
			<td><select class='repTblCell form-control' name='fieldRepScriptID' value='' >";
			
	if($fyl=opendir("../scripts")):

		while(false !==($file=readdir($fyl))):
			$ftxt=preg_split("/\./",$file);
			if($ftxt[sizeof($ftxt)-1]=="xml" && sizeof($ftxt)>1 && $ftxt[0]!="draft"):
				$content.="<option value=\"".$ftxt[0]."\">".$ftxt[0]."</option>";
			endif;
		endwhile;	
	endif;
			$content.= "</select></td>
			<td><select name='fieldRepStatus' class='form-control'><option value='ACTIVE'>Active</option><option value='INACTIVE'>Inactive</option></select>	</td>
			<td><input type='button' class='repTblCell repTblButton btn btn-primary' value='Add' onclick='repAdd();' /><input type='hidden' name='fieldSubmitter' value='".SAVANTIUS."' /></td>
			</form>
		</tr>
		";
		$altRow=0;
		$i=0;
		openDb();
		$result= mysql_query("SELECT * FROM `nonvoice_agc` WHERE NOT `status`='DELETED' ORDER BY `status` ASC");
		closeDb();
		while($row=@mysql_fetch_array($result))
		{
			$recID=$row['id'];
			$repStatus=$row['status'];
			$repName=$row['Name'];
			$repID=$row['userid'];
			$repPW=$row['password'];
			$repScriptID=$row['script_id'];
			if ($altRow)
			{
				$content.="<tr>";
				$altRow=0;
			}
			else
			{
				$content.="<tr >";
				$altRow=1;
			}
			$content.="
				<form name='formUpdateRep".$i."' method='post' action='./?a=".obfuscateActionID('ADMIN_REP_UPDATE_SUBMIT')."'>
				<td><input type='text' class='repTblCell' name='fieldRepName' value='".$repName."' /></td>
				<td><input type='text' class='repTblCell' name='fieldRepID' value='".$repID."' /></td>
				<td><input type='text' class='repTblCell' name='fieldRepPW' value='".$repPW."' /></td>
				<td><select class='repTblCell' name='fieldRepScriptID'>";
				
	if($fyl=opendir("../scripts")):

		while(false !==($file=readdir($fyl))):
			$ftxt=preg_split("/\./",$file);
			if($ftxt[sizeof($ftxt)-1]=="xml" && sizeof($ftxt)>1 && $ftxt[0]!="draft"):
				$content.="<option ".(($ftxt[0]==$repScriptID)?"selected":"")." value=\"".$ftxt[0]."\">".$ftxt[0]."</option>";
			endif;
		endwhile;	
	endif;
				$content.="</select></td>
				<td>
					<select name='fieldRepStatus'>";
					if($repStatus=="ACTIVE")
					{
						$content.="<option value='ACTIVE' selected>Active</option><option value='INACTIVE'>Inactive</option><option value='DELETED'>Deleted</option>";
					}
					else
					if($repStatus=="INACTIVE")
					{
						$content.="<option value='ACTIVE'>Active</option><option value='INACTIVE' selected>Inactive</option><option value='DELETED'>Deleted</option>";
					}
					$content.="
					</select>
				</td>
				<td><input type='button' class='repTblCell repTblButton btn btn-info' value='Update' onclick='repUpdate("; $content.='"formUpdateRep'.$i.'"'; $content.=");' /><input type='hidden' name='fieldSubmitter' value='".SAVANTIUS."' /><input type='hidden' name='fieldRecID' value='".$recID."' /></td>
				</form>
			</tr>
			";
			$i++;
		}
	$content.="
	</table>
	";
	return($content);
}

function refreshRepList()
{
	echo "
	<html>
	<head>
	<title></title>
	<meta http-equiv='refresh' content='0;URL=./'>
	</head>
	<body>
	</body>
	</html>
	";
}

$actionID=deobfuscateActionID((int)$_GET['a']);
switch ($actionID)
{
	case "ADMIN_REP_ADD_SUBMIT":
		repAdd();
		refreshRepList();
		break;
	case "ADMIN_REP_UPDATE_SUBMIT":
		repUpdate();
		refreshRepList();
		break;
	default:
		$content=showRepList();
}

?>
